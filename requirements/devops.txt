pyyaml==6.0.1
cli-ui==0.17.2
ansible==9.4.0
kubernetes==26.1.0
jmespath==1.0.1