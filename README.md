## Подготовка к работе
1. Корневая папка должна иметь права 775

## Настройка кластера rke2
Кластрер rke2 служит базой для rancher. В идеале запускать на 3-х серверах, но можно настроить и на одном(страдает отказоустойчивость)
Установка кластера происходит сначала на один сервер, а потом подключаються все остальные. После чего устанавливаеться rancher.

### Запуск установки rke2 кластера
1. `ansible-playbook -i devops/ansible/apps/rancher/inventory.yml devops/ansible/apps/rancher/playbooks/rke.yml`

### Деплой rancher
1. `ansible-playbook -i devops/ansible/apps/rancher/inventory.yml devops/ansible/apps/rancher/playbooks/rancher.yml`

### Деплой argocd
1. `ansible-playbook -i devops/ansible/apps/rancher/inventory.yml devops/ansible/apps/rancher/playbooks/argocd.yml`