import os
from .base import *
from distutils.util import strtobool

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('BACKEND_DATABASE_NAME'),
        'USER': os.environ.get('BACKEND_DATABASE_USER'),
        'PASSWORD': os.environ.get('BACKEND_DATABASE_PASSWORD'),
        'HOST': os.environ.get('BACKEND_DATABASE_HOST'),
        'PORT': os.environ.get('BACKEND_DATABASE_PORT'),
    },
}

ALLOWED_HOSTS = ['*']
DEBUG = strtobool(os.environ.get('BACKEND_DEBUG', 'false'))

#Celery settings
CELERY_REDIS_HOST = os.environ.get('BACKEND_CELERY_REDIS_HOST', '127.0.0.1')
CELERY_REDIS_PORT = os.environ.get('BACKEND_CELERY_REDIS_PORT', 6379)
CELERY_REDIS_DB = os.environ.get('BACKEND_CELERY_REDIS_DATABASE', 3)
BROKER_URL = 'redis://%s:%s/%s' % (CELERY_REDIS_HOST, CELERY_REDIS_PORT, CELERY_REDIS_DB)
CELERY_RESULT_BACKEND = BROKER_URL
